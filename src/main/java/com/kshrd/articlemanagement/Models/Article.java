package com.kshrd.articlemanagement.Models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Article {

    // attributes in article
    private int id;
    @NotBlank
    private String name;
    @NotBlank
    private String description;
    @NotBlank
    @Size(min=5, max=10)
    private String author;
    private Category category;
    private String thumnail;
    private String createdDate;


    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getThumnail() {
        return thumnail;
    }

    public void setThumnail(String thumnail) {
        this.thumnail = thumnail;
    }

    public void setId(int id) {
        this.id=id;
    }
    public int getId() {
        return id;
    }


    public void setName(String name) {
        this.name=name;
    }
    public String getName() {
        return name;
    }


    public void setDescription(String descriptioin) {
        this.description=descriptioin;
    }
    public String getDescription() {
        return description;
    }


    public void setAuthor(String author) {
        this.author=author;
    }
    public String getAuthor() {
        return author;
    }


    public void setCreatedDate(String createdDate) {
        this.createdDate=createdDate;
    }
    public String getCreatedDate() {
        return createdDate;
    }

    public Article() {}

    public Article(int id, @NotBlank String name, @NotBlank String description, @NotBlank @Size(min = 5, max = 10) String author, String thumnail, Category category, String createdDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.author = author;
        this.thumnail = thumnail;
        this.category = category;
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", category=" + category +
                ", thumnail='" + thumnail + '\'' +
                ", createdDate='" + createdDate + '\'' +
                '}';
    }
}
