package com.kshrd.articlemanagement.configurations;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfiguration {

    @Bean("dataSource")
    @Profile("postgresql")
    public DataSource postgresql(){
        DriverManagerDataSource db=new DriverManagerDataSource();

        db.setDriverClassName("org.postgresql.Driver");
        db.setUrl("jdbc:postgresql://localhost:5432/articlemanagement");
        db.setUsername("postgres");
        db.setPassword("123");
        return db;
    }

    @Bean("dataSource")
    @Profile("memory")
    public DataSource h2Memory(){
        EmbeddedDatabaseBuilder db=new EmbeddedDatabaseBuilder();
        db.addScript("db/schema.sql");
        db.addScript("db/data.sql");
        db.setType(EmbeddedDatabaseType.H2);

        return db.build();
    }
}
