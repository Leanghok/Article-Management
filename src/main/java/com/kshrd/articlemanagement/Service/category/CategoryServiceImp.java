package com.kshrd.articlemanagement.Service.category;

import com.kshrd.articlemanagement.Models.Category;
import com.kshrd.articlemanagement.Repository.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepo;

    @Override
    public List<Category> findAll() {
        return categoryRepo.findAll();
    }

    @Override
    public Category findOne(int id) {

        return categoryRepo.findOne(id);
    }

    @Override
    public void insertCategory(Category category) {
        categoryRepo.insertCategory(category);
    }

    @Override
    public void deleteCategory(int id) {
        categoryRepo.deleteCategory(id);
    }

    @Override
    public void updateCategory(Category category) {
        categoryRepo.updateCategory(category);
    }
}
