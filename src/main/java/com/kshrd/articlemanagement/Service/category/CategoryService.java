package com.kshrd.articlemanagement.Service.category;

import com.kshrd.articlemanagement.Models.Category;

import java.util.List;

public interface CategoryService {

    List<Category> findAll();
    Category findOne(int id);
    void insertCategory(Category category);
    void deleteCategory(int id);
    void updateCategory(Category category);
}
