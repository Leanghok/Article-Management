package com.kshrd.articlemanagement.Service.article;

import com.kshrd.articlemanagement.Models.Article;

import java.util.List;

public interface ArticleService {
    void add(Article article);
    Article find(int id);
    List<Article> findAll();
    void delete(int id);
    void update(Article article);
}
