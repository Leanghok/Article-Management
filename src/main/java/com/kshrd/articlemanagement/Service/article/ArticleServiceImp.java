package com.kshrd.articlemanagement.Service.article;

import com.kshrd.articlemanagement.Models.Article;
import com.kshrd.articlemanagement.Repository.article.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService{
    @Autowired
    private ArticleRepository articleRepo;

    @Override
    public void add(Article article) {
        articleRepo.add(article);
    }

    @Override
    public Article find(int id) {
        return articleRepo.find(id);
    }

    @Override
    public List<Article> findAll() {
        return articleRepo.findAll();
    }

    @Override
    public void delete(int id) {
        articleRepo.delete(id);

    }

    @Override
    public void update(Article article) {
        articleRepo.update(article);
    }
}
