package com.kshrd.articlemanagement.Repository.category;

import com.kshrd.articlemanagement.Models.Category;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @Select("SELECT id, name FROM tbl_categories ORDER BY id ASC")
    List<Category> findAll();
    @Select("SELECT id, name FROM tbl_categories where id=#{id}")
    Category findOne(int id);

    @Insert("Insert into tbl_categories (name) values(#{name})")
    void insertCategory(Category category);

    @Insert("Delete from tbl_categories where id=#{id}")
    void deleteCategory(int id);

    @Update("Update tbl_categories set name=#{name} where id=#{id}")
    void updateCategory(Category category);
}
