package com.kshrd.articlemanagement.Repository.category;

import com.kshrd.articlemanagement.Models.Category;

import java.util.ArrayList;
import java.util.List;

//@Repository
public class CategoryRepositoryIpm implements CategoryRepository {

    private List<Category> categories=new ArrayList<>();

    public CategoryRepositoryIpm(){
        categories.add(new Category(1,"Spring"));
        categories.add(new Category(2,"Java"));
        categories.add(new Category(3,"Web"));
        categories.add(new Category(4,"Database"));
    }

    @Override
    public List<Category> findAll() {

        return categories;
    }

    @Override
    public Category findOne(int id) {
        for(Category c:categories){
            if(c.getId()==id){
                return c;
            }
        }
        return null;
    }

    @Override
    public void insertCategory(Category category) {

    }

    @Override
    public void deleteCategory(int id) {

    }

    @Override
    public void updateCategory(Category category) {

    }
}
