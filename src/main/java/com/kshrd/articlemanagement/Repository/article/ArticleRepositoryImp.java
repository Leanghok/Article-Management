package com.kshrd.articlemanagement.Repository.article;

import com.github.javafaker.Faker;
import com.kshrd.articlemanagement.Models.Article;
import com.kshrd.articlemanagement.Models.Category;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//@Repository
public class ArticleRepositoryImp implements ArticleRepository{
    private List<Article> articles= new ArrayList<>();
    ArticleRepositoryImp(){
        Faker faker=new Faker();
        for(int i=0;i<10;i++){
            articles.add(new Article(i,faker.lastName(),faker.firstName(),faker.country(),"KSHRD.jpg",new Category(3,"Web"),new Date().toString()));
        }
    }
    @Override
    public void add(Article article) {
        articles.add(article);
    }

    @Override
    public Article find(int id) {
         for(Article article:articles){
             if(article.getId()==id)
                 return article;
         }
        return null;
    }

    @Override
    public List<Article> findAll() {
        return articles;
    }

    @Override
    public void delete(int id) {
        for(Article article : articles){
            if(article.getId()==id) {
                articles.remove(article);
                break;
            }
        }
        return;
    }

    @Override
    public void update(Article article) {
        for(int i=0; i<articles.size();i++){
            if (articles.get(i).getId()==article.getId()){
                articles.get(i).setName(article.getName());
                articles.get(i).setAuthor(article.getAuthor());
                articles.get(i).setDescription(article.getDescription());
                articles.get(i).setCategory(article.getCategory());
                articles.get(i).setThumnail(article.getThumnail());
            }
        }
    }
}
