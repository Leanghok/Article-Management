package com.kshrd.articlemanagement.Repository.article;

import com.kshrd.articlemanagement.Models.Article;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {

    @Insert("Insert into tbl_articles(title,description,author,created_date,thumbnail,category_id) values(#{name},#{description},#{author},#{createdDate},#{thumnail},#{category.id})")
    void add(Article article);

    @Select("Select a.id,a.title,a.description,a.author,a.created_date,a.thumbnail,a.category_id,c.name category_name from tbl_articles a inner join tbl_categories c on a.category_id=c.id where a.id=#{id}")
    @Results({
            @Result(property="id",column = "id"),
            @Result(property="name",column = "title"),
            @Result(property="description",column = "description"),
            @Result(property="author",column = "author"),
            @Result(property="createdDate",column = "created_date"),
            @Result(property="thumnail",column = "thumbnail"),
            @Result(property="category.id",column = "category_id"),
            @Result(property="category.name",column = "category_name")
    })
    Article find(int id);

    @Select("Select a.id,a.title,a.description,a.author,a.created_date,a.thumbnail,a.category_id,c.name as category_name from tbl_articles a inner join tbl_categories c on a.category_id=c.id order by a.id ASC")
    @Results({
            @Result(property="name",column = "title"),
            @Result(property="createdDate",column = "created_date"),
            @Result(property="thumnail",column = "thumbnail"),
            @Result(property="category.id",column = "category_id"),
            @Result(property="category.name",column = "category_name")
    })
    List<Article> findAll();

    @Delete("DELETE FROM tbl_articles WHERE id=#{id}")
    void delete(int id);

    @Update("UPDATE tbl_articles SET title=#{name}, description=#{description}, author=#{author},category_id=#{category.id},thumbnail=#{thumnail} WHERE id=#{id}")
    void update(Article article);
}
