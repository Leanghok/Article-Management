package com.kshrd.articlemanagement.Controller;

import com.kshrd.articlemanagement.Models.Article;
import com.kshrd.articlemanagement.Service.article.ArticleService;
import com.kshrd.articlemanagement.Service.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Controller
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private CategoryService categoryService;
    @GetMapping({"/article" , "/" , "home"})
    public String article(ModelMap m){
        m.addAttribute("articles", articleService.findAll());
        return "article/article";
    }


    @GetMapping("/add")
    public String add(ModelMap m){
        m.addAttribute("article",new Article());
        m.addAttribute("categories",categoryService.findAll());
        m.addAttribute("formAdd",true);
        return "article/add";
    }

    @PostMapping("/add")
    public String saveArticle(@Valid @ModelAttribute Article article, BindingResult result, ModelMap modelMap, @RequestParam("file") MultipartFile file) {
        if(result.hasErrors()) {
            modelMap.addAttribute("article", article);
            modelMap.addAttribute("formAdd", true);
            return "article/add";
        }
        String serverPath = "/Users/macbookpro/Desktop/ArticleManagement/image/";
        if(!file.isEmpty()) {
            String fileName = UUID.randomUUID() + "." + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath, fileName));
                article.setThumnail(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            article.setThumnail("None");
        }
        article.setCategory(categoryService.findOne(article.getCategory().getId()));
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
        Date today=new Date();
        String date=DATE_FORMAT.format(today);
        article.setCreatedDate(date);
        articleService.add(article);
        return "redirect:/article";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id){
        articleService.delete(id);
        return "redirect:/article";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable int id, ModelMap m){
        m.addAttribute("article",articleService.find(id));
        m.addAttribute("categories",categoryService.findAll());
        m.addAttribute("formAdd",false);
        return "article/add";
    }

    @PostMapping("/update")
    public String saveUpdate(@ModelAttribute Article article, BindingResult result, ModelMap modelMap, @RequestParam("file") MultipartFile file){
        article.setCreatedDate(new Date().toString());
        String serverPath = "/Users/macbookpro/Desktop/ArticleManagement/image/";
        if(!file.isEmpty()) {
            String fileName = UUID.randomUUID() + "." + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath, fileName));
                article.setThumnail(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            article.setThumnail(articleService.find(article.getId()).getThumnail());
        }
        System.out.println(article.getThumnail());
        article.setCategory(categoryService.findOne(article.getCategory().getId()));
        System.out.println(article.getCategory());
        articleService.update(article);
        return "redirect:/article";
    }

    @GetMapping("/view/{id}")
    public String viewArticle(ModelMap map, @PathVariable("id") int id){
        map.addAttribute("article",articleService.find(id));

        return "article/view";
    }



}
