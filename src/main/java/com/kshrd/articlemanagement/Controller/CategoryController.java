package com.kshrd.articlemanagement.Controller;

import com.kshrd.articlemanagement.Models.Category;
import com.kshrd.articlemanagement.Service.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/category")
    public String category(ModelMap cate){
        cate.addAttribute("categories",categoryService.findAll());
        return "category/category";
    }

    @GetMapping("/category/add")
    public String getAddCategory(ModelMap cate){
        cate.addAttribute("category",new Category());
        cate.addAttribute("checkAdd", true);
        return "category/add";
    }

    @PostMapping("/category/add")
    public String postAddCategory(@Valid @ModelAttribute Category category){
        categoryService.insertCategory(category);
        return "redirect:/category";
    }

    @GetMapping("/category/delete/{id}")
    public String getDeleteCategory(@PathVariable("id") int id){
        categoryService.deleteCategory(id);

        return "redirect:/category";
    }

    @GetMapping("/category/update/{id}")
    public String getUpdateCategory(ModelMap cate, @PathVariable("id") int id){
        cate.addAttribute("category",categoryService.findOne(id));
        return "/category/add";
    }

    @PostMapping("/category/update")
    public String postUpdateCategory(@ModelAttribute Category category){
        System.out.println(category);
        categoryService.updateCategory(category);
        return "redirect:/category";
    }
}
