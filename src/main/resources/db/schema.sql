create table tbl_categories
(
  id   serial      not null
    constraint category_pkey
    primary key,
  name varchar(50) not null
);

create table tbl_articles
(
  id           serial       not null
    constraint tbl_articles_pkey
    primary key,
  title        varchar(50)  not null,
  description  text         not null,
  author       varchar(30)  not null,
  created_date varchar(30),
  thumbnail    varchar(100) not null,
  category_id  integer
    constraint tbl_articles_category_id_fkey
    references tbl_categories
    on update cascade on delete cascade
);

